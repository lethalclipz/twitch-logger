package main

import (
	"math/rand"
	"strconv"
	"strings"
	"time"

	"github.com/BurntSushi/toml"
	colorable "github.com/mattn/go-colorable"
	log "github.com/sirupsen/logrus"
	irc "github.com/thoj/go-ircevent"
	"gopkg.in/mgo.v2"

	"gitlab.com/lethalclipz/twitch-logger/types"
	"gitlab.com/lethalclipz/twitch-logger/webapi"
)

var db *mgo.Database

type Config struct {
	Channels []string
}

func main() {
	log.SetFormatter(&log.TextFormatter{ForceColors: true})
	log.SetOutput(colorable.NewColorableStdout())

	var config Config
	if _, err := toml.DecodeFile("settings.toml", &config); err != nil {
		log.WithError(err).Panic("Failed to read config file")
	}

	session, err := mgo.Dial("localhost")
	if err != nil {
		log.WithError(err).Panic("Failed to connect to database")
	}

	db = session.DB("messages")

	if err != nil {
		log.WithError(err).Error("Failed to create messages bucket")
	}

	go webapi.StartServer(db)

	rand.Seed(int64(time.Now().Nanosecond()))
	username := "justinfan" + strconv.Itoa(rand.Intn(1000000000))
	log.WithField("username", username).Info("Connecting to IRC server")
	ircConnection := irc.IRC(username, username)

	ircConnection.AddCallback("PRIVMSG", handleMessage)
	ircConnection.AddCallback("CTCP_ACTION", handleMessage)
	ircConnection.AddCallback("001", func(e *irc.Event) {
		for _, v := range config.Channels {
			ircConnection.Join("#" + v)
			log.WithField("channel", v).Info("Joined channel")
		}
	})

	err = ircConnection.Connect("irc.twitch.tv:6667")
	if err != nil {
		log.WithError(err).Error("Failed to connect to IRC server")
	}

	ircConnection.Loop()
}

func handleMessage(event *irc.Event) {
	go func(event *irc.Event) {
		channel := strings.ToLower(strings.TrimPrefix(event.Arguments[0], "#"))
		messageString := event.Message()

		// Handling for "/me" messages (different IRC event code)
		if event.Code == "CTCP_ACTION" {
			messageString = "/me " + messageString
		}

		log.WithFields(log.Fields{
			"channel": channel,
			"user":    strings.ToLower(event.Nick),
			"content": messageString,
			"code":    event.Code,
		}).Info("Message received")

		channelCollection := db.C(channel)

		index := mgo.Index{
			Key: []string{"-timestamp"},
		}
		err := channelCollection.EnsureIndex(index)
		if err != nil {
			log.WithError(err).Error("Failed to ensure timestamp index")
		}

		index = mgo.Index{
			Key: []string{"author", "-timestamp"},
		}
		err = channelCollection.EnsureIndex(index)
		if err != nil {
			log.WithError(err).Error("Failed to ensure author-timestamp index")
		}

		message := &types.Message{Channel: channel, Author: event.Nick, Content: messageString, Timestamp: time.Now().Format(time.RFC3339Nano)}

		err = channelCollection.Insert(message)
		if err != nil {
			log.WithError(err).Error("Failed to insert message into database")
		}
	}(event)
}
