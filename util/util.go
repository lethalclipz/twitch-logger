package util

import (
	"errors"
	"net/http"
	"strconv"
	"strings"

	"github.com/gin-gonic/gin"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	
	"gitlab.com/lethalclipz/twitch-logger/types"
)

func StringIsInSlice(match string, slice []string) bool {
	for _, v := range slice {
		if match == v {
			return true
		}
	}
	return false
}

func GetLimitFromRequest(c *gin.Context) (int, error) {
	limitString := c.DefaultQuery("limit", "500")
	limit, err := strconv.Atoi(limitString)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Limit must be a number.",
		})
		return -1, err
	}

	if limit < 1 || limit > 2000 {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Number must be between 1 and 2000.",
		})
		return -1, errors.New("Number must be between 1 and 2000.")
	}

	return limit, nil
}

func GetMessagesFromChannel(db *mgo.Database, channel string, limit int, filter bson.M) ([]types.Message, error) {
	channel = strings.ToLower(channel)
	collection := db.C(channel)

	var messages []types.Message
	query := collection.Find(filter).Limit(limit).Sort("-timestamp")

	err := query.Iter().All(&messages)
	if err != nil {
		return []types.Message{}, err
	}

	return messages, nil
}

func ChannelIsInDatabase(db *mgo.Database, channel string) (bool, error) {
	channel = strings.ToLower(channel)
	channels, err := db.CollectionNames()
	if err != nil {
		return false, err
	}

	return StringIsInSlice(channel, channels), nil
}
