package types

type Message struct {
	Author    string
	Channel   string
	Content   string
	Timestamp string
}
