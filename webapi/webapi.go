package webapi

import (
	"net/http"
	"strings"

	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"

	"gitlab.com/lethalclipz/twitch-logger/util"
)

var db *mgo.Database

func StartServer(dbConn *mgo.Database) {
	db = dbConn

	gin.DisableConsoleColor()
	engine := gin.Default()
	engine.Use(gzip.Gzip(gzip.DefaultCompression))

	engine.GET("/channels", getListOfChannelsWatched)
	engine.GET("/channels/:channel/messages", getMessagesInChannel)
	engine.GET("/channels/:channel/messages/users/:user", getMessagesInChannelForUser)

	engine.Run(":8080")
}

func getListOfChannelsWatched(c *gin.Context) {
	channels, err := db.CollectionNames()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(200, channels)
}

func getMessagesInChannel(c *gin.Context) {
	channel := c.Param("channel")
	channelIsInDB, err := util.ChannelIsInDatabase(db, channel)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	if !channelIsInDB {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Channel " + channel + " not found",
		})
		return
	}

	limit, err := util.GetLimitFromRequest(c)
	if err != nil {
		return
	}

	messages, err := util.GetMessagesFromChannel(db, channel, limit, bson.M{})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	c.JSON(http.StatusOK, messages)
}

func getMessagesInChannelForUser(c *gin.Context) {
	channel := c.Param("channel")
	channelIsInDB, err := util.ChannelIsInDatabase(db, channel)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		return
	}

	if !channelIsInDB {
		c.JSON(http.StatusNotFound, gin.H{
			"error": "Channel " + channel + " not found",
		})
		return
	}

	limit, err := util.GetLimitFromRequest(c)
	if err != nil {
		return
	}

	messages, err := util.GetMessagesFromChannel(db, channel, limit, bson.M{"author": strings.ToLower(c.Param("user"))})
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
	}

	c.JSON(http.StatusOK, messages)
}
